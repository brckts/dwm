/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 1;        /* 1 means no outer gap when there is only one window */
static const unsigned int systraypinning = 1;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const unsigned int activetagindicatorheight = 2;
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 20;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "DejaVuSansMono Nerd Font:size=9" };
static const char dmenufont[]       = "DejaVuSansMono Nerd Font:size=9";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char urgfgcolor[]            = "#eeeeee";
static char urgbordercolor[]        = "#005577";
static char urgbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeUrg]  = { urgfgcolor,  urgbgcolor,  urgbordercolor  },
};

/* tagging */
static const char *tags[] =    { " ", " ", " ", " ", " ", " ", " ", " ", " ", " " };
static const char *tagsalt[] = { "" , "" , "" , "" , "" , "" , "" , "" , "" , "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	/* 1 is left, 2 is right, 0 is probably primary */
	{ "mpv",      NULL,       NULL,       0,            1,           -1 },
	{ NULL,       "Navigator",NULL,       1 << 1,       0,            1 },
	{ "discord",  NULL,       NULL,       1 << 2,       0,            2 },
	{ "Steam",    NULL,       NULL,       1 << 3,       0,            0 },
	{ "Deluge",   NULL,       NULL,       1 << 8,       0,            1 },
	{ NULL,	      NULL,       "Origin",        0,       1,            0 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	// { "[]=",      tile },    [> first entry is default <]
	{ "[]=",      tile },    /* [> first entry is default <] */
	// { "><>",      NULL },    [> no layout function means floating behavior <]
	{ "穀",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
#include <X11/XF86keysym.h>

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", "-e", "ddterm",  NULL };

static Key keys[] = {
	/* modifier                     key                         function        argument */
	{ MODKEY,						XK_d,                       spawn,          {.v = dmenucmd } },
	// { MODKEY,						XK_b,                   togglebar,      {0} },
	{ MODKEY,						XK_j,                       focusstack,     {.i = +1 } },
	{ MODKEY,						XK_k,                       focusstack,     {.i = -1 } },
	{ MODKEY,						XK_p,                       incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,				XK_p,                       incnmaster,     {.i = -1 } },
	{ MODKEY,						XK_h,                       setmfact,       {.f = -0.05} },
	{ MODKEY,						XK_l,                       setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,           XK_r,                       togglegaps,     {0} },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	{ MODKEY,						XK_g, 	                    zoom,           {0} },
	{ MODKEY,						XK_Tab,                     view,           {0} },
	{ MODKEY,		                XK_q,                       killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_q,                       spawn,          SHCMD("kill -s KILL $(xprop -id `xdotool getwindowfocus` | grep '_NET_WM_PID' | grep -oE '[[:digit:]]*$')") },
	{ MODKEY,                       XK_t,                       setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,                       setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,                       setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_u,                       setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_o,                       setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,                   setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,                   togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,	                    togglefullscr,  {0} },
	{ MODKEY,                       XK_0,                       view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,                       tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_semicolon,               focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_comma,                   focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_semicolon,               tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_comma,                   tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_n,                       togglealttag,   {0} },
	{ MODKEY,                       XK_z,                       togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_F5,                      xrdb,           {.v = NULL } },
	TAGKEYS(                        XK_ampersand,				0)
	TAGKEYS(                        XK_eacute,					1)
	TAGKEYS(                        XK_quotedbl,				2)
	TAGKEYS(                        XK_apostrophe,				3)
	TAGKEYS(                        XK_parenleft,				4)
	TAGKEYS(                        XK_minus,					5)
	TAGKEYS(                        XK_egrave,					6)
	TAGKEYS(                        XK_underscore,				7)
	TAGKEYS(                        XK_ccedilla,				8)
	TAGKEYS(                        XK_agrave,					9)
	{ MODKEY|ControlMask|ShiftMask, XK_q,                       quit,           {0} },
	{ MODKEY,                       XK_x,                       spawn,          SHCMD("slock -m \"$(cowsay \"$(fortune)\")\"") },
	{ MODKEY,                       XK_Return,                  spawn,          SHCMD("$DWMTERM") },
	{ MODKEY|ShiftMask,				XK_m,                       spawn,          SHCMD("$DWMTERM -e ncmpcpp") },
	{ MODKEY|ShiftMask,				XK_o,                       spawn,          SHCMD("$DWMTERM -e calcurse") },
	{ MODKEY|ShiftMask,             XK_Return,                  spawn,          SHCMD("samedir") },
	{ MODKEY,                       XK_r,                       spawn,          SHCMD("$DWMTERM -e $FILE") },
	{ MODKEY|ShiftMask,             XK_r,                       spawn,          SHCMD("$GRAPH_FILE") },
	{ MODKEY,                       XK_v,                       spawn,          SHCMD("$DWMTERM -e $EDITOR") },
	{ ShiftMask,                    XK_Print,                   spawn,          SHCMD("maimpick") },
	{ MODKEY,                       XK_i,                       spawn,          SHCMD("$DWMTERM -e gotop -p") },
	{ MODKEY,                       XK_c,                       spawn,          SHCMD("confedit") },
	{ MODKEY|ShiftMask,             XK_c,                       spawn,          SHCMD("$DWMTERM -e scriptedit") },
	{ MODKEY|ShiftMask,             XK_a,                       spawn,          SHCMD("pavucontrol-qt") },
	{ MODKEY|ShiftMask,             XK_Escape,                  spawn,          SHCMD("dwmlogout") },
	{ 0,                            XF86XK_AudioMute,           spawn,          SHCMD("lmc mute") },
	{ 0,                            XF86XK_AudioLowerVolume,    spawn,          SHCMD("lmc down 5") },
	{ ControlMask,                  XF86XK_AudioLowerVolume,    spawn,          SHCMD("lmc down 1") },
	{ 0,                            XF86XK_AudioRaiseVolume,    spawn,          SHCMD("lmc up 5") },
	{ ControlMask,                  XF86XK_AudioRaiseVolume,    spawn,          SHCMD("lmc up 1") },
	{ 0,                            XF86XK_AudioPlay,           spawn,          SHCMD("lmc toggle") },
	{ 0,                            XF86XK_AudioStop,           spawn,          SHCMD("mpc stop") },
	{ 0,                            XF86XK_AudioNext,           spawn,          SHCMD("lmc next") },
	{ 0,                            XF86XK_AudioPrev,           spawn,          SHCMD("lmc prev") },
	{ 0,                            XF86XK_AudioRewind,         spawn,          SHCMD("lmc back 10") },
	{ 0,                            XF86XK_AudioForward,        spawn,          SHCMD("lmc forward 10") },
	{ 0,                            XF86XK_MonBrightnessUp,     spawn,          SHCMD("xbacklight -inc 5") },
	{ 0,                            XF86XK_MonBrightnessDown,   spawn,          SHCMD("xbacklight -dec 5") },
	{ ControlMask,                  XF86XK_MonBrightnessUp,	    spawn,          SHCMD("xbacklight -inc 1") },
	{ ControlMask,                  XF86XK_MonBrightnessDown,   spawn,          SHCMD("xbacklight -dec 1") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

